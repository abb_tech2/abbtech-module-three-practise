package com.abbtech.practice.exercise18.resository;

import com.abbtech.practice.exercise18.model.TaskEntity;

import java.util.List;
import java.util.Optional;

public interface TaskRepository {
    void createTasks(List<TaskEntity> tasks);
    void save(TaskEntity task);
    void update(TaskEntity task);
    void delete(Long id);
    Optional<List<TaskEntity>> getAll();
    Optional<TaskEntity> getById(Long id);
}
