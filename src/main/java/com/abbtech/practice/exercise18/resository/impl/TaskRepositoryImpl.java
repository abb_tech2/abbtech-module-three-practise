package com.abbtech.practice.exercise18.resository.impl;

import com.abbtech.practice.exercise18.enumeration.Status;
import com.abbtech.practice.exercise18.model.TaskEntity;
import com.abbtech.practice.exercise18.resository.TaskRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;
import java.util.Optional;

@Repository
public class TaskRepositoryImpl implements TaskRepository {
    private final JdbcTemplate jdbcTemplate;

    public TaskRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SELECT_TASK_BY_ID_SQL = "SELECT id, name, description, due_date, status FROM tasks WHERE id = ?";
    private static final String SELECT_TASK_SQL = "SELECT id, name, description, due_date, status FROM tasks";
    private static final String INSERT_TASK_SQL = "INSERT INTO tasks (name, description, due_date, status) VALUES (?, ?, ?, ?)";
    private static final String UPDATE_TASK_SQL = "UPDATE tasks SET name = ?, description = ?, due_date = ?, status = ? WHERE id = ?";
    private static final String DELETE_TASK_SQL = "DELETE FROM tasks WHERE id = ?";


    public void createTasks(List<TaskEntity> tasks) {
        jdbcTemplate.batchUpdate(INSERT_TASK_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                TaskEntity task = tasks.get(i);
                ps.setString(1, task.getName());
                ps.setString(2, task.getDescription());
                ps.setDate(3, new Date(task.getDueDate().getTime()));
                ps.setString(4, task.getStatus().toString());
            }

            @Override
            public int getBatchSize() {
                return tasks.size();
            }
        });
    }

    public Optional<List<TaskEntity>> getAll() {
        try {
            return Optional.of(jdbcTemplate.query(SELECT_TASK_SQL, this::mapRowToTaskEntity));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<TaskEntity> getById(Long id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_TASK_BY_ID_SQL, new Object[]{id}, this::mapRowToTaskEntity));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
    }

    @Override
    public void save(TaskEntity task) {
        jdbcTemplate.update(INSERT_TASK_SQL, task.getName(), task.getDescription(), task.getDueDate(), task.getStatus().toString());
    }

    @Override
    public void update(TaskEntity task) {
        jdbcTemplate.update(UPDATE_TASK_SQL, task.getName(), task.getDescription(), task.getDueDate(), task.getStatus().toString(), task.getId());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(DELETE_TASK_SQL, id);
    }

    private TaskEntity mapRowToTaskEntity(ResultSet rs, int rowNum) throws SQLException {
        Long taskId = rs.getLong("id");
        String name = rs.getString("name");
        String description = rs.getString("description");
        Date dueDate = rs.getDate("due_date");
        Status status = Status.valueOf(rs.getString("status"));
        return new TaskEntity(taskId, name, description, dueDate, status);
    }
}