package com.abbtech.practice.exercise18.enumeration;

public enum Status {
    PENDING,
    IN_PROGRESS,
    DONE
}
