package com.abbtech.practice.exercise18.services;

import com.abbtech.practice.exercise18.model.TaskEntity;

import java.util.List;

public interface TaskService {
    void createTasks(List<TaskEntity> tasks);
    void save(TaskEntity task);
    void update(TaskEntity task);
    void delete(Long id);
    List<TaskEntity> getAll();
    TaskEntity getById(Long id);
}
