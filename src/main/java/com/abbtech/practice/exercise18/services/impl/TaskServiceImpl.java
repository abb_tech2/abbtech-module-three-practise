package com.abbtech.practice.exercise18.services.impl;

import com.abbtech.practice.exercise18.model.TaskEntity;
import com.abbtech.practice.exercise18.resository.TaskRepository;
import com.abbtech.practice.exercise18.services.TaskService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void createTasks(List<TaskEntity> tasks) {
        taskRepository.createTasks(tasks);
    }

    @Override
    public List<TaskEntity> getAll() {
        return taskRepository.getAll().orElse(Collections.emptyList());
    }

    @Override
    public TaskEntity getById(Long id) {
        return taskRepository.getById(id).orElse(null);
    }

    @Override
    public void save(TaskEntity task) {
        taskRepository.save(task);
    }

    @Override
    public void update(TaskEntity task) {
        taskRepository.update(task);
    }

    @Override
    public void delete(Long id) {
        taskRepository.delete(id);
    }

}
