package com.abbtech.practice.exercise18.model;

import com.abbtech.practice.exercise18.enumeration.Status;

import java.util.Date;
public class TaskEntity {
    private Long id;
    private String name;
    private String description; 
    private Date dueDate;
    private Status status;

    public TaskEntity(Long id, String name, String description, Date dueDate, Status status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.status = status;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Status getStatus() {
        return status;
    }

}
