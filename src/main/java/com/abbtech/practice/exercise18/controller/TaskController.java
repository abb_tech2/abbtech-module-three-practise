package com.abbtech.practice.exercise18.controller;

import com.abbtech.practice.exercise18.model.TaskEntity;
import com.abbtech.practice.exercise18.services.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/list")
    public void createTasks(@RequestBody List<TaskEntity> task) {
        taskService.createTasks(task);
    }

    @GetMapping
    public List<TaskEntity> getAll() {
        return taskService.getAll();
    }

    @GetMapping("/{id}")
    public TaskEntity getTaskById(@PathVariable Long id) {
        return taskService.getById(id);
    }

    @PostMapping
    public void save(@RequestBody TaskEntity task) {
        taskService.save(task);
    }

    @PutMapping
    public void update(@RequestBody TaskEntity task) {
        taskService.update(task);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        taskService.delete(id);
    }
}
