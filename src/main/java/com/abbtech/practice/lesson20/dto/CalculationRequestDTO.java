package com.abbtech.practice.lesson20.dto;

import com.abbtech.practice.lesson20.enumeration.OperationEnum;

public record CalculationRequestDTO(Integer a, Integer b, OperationEnum operation) {
}
