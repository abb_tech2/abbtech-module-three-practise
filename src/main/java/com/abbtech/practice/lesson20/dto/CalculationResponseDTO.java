package com.abbtech.practice.lesson20.dto;

public record CalculationResponseDTO(Integer result) {
}
