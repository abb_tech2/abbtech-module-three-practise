package com.abbtech.practice.lesson20.service;

public interface CalculatorService {
    int multiply(int a, int b);

    int subtract(int a, int b);

    int addition(int a, int b);

    int division(int a, int b);
}
