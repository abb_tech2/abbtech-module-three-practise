package com.abbtech.practice.lesson20.service.impl;

import com.abbtech.practice.lesson20.service.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int addition(int a, int b) {
        return a + b;
    }

    @Override
    public int division(int a, int b) {
        return a/b;
    }
}
