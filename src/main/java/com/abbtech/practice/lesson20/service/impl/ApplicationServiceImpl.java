package com.abbtech.practice.lesson20.service.impl;

import com.abbtech.practice.lesson20.enumeration.OperationEnum;
import com.abbtech.practice.lesson20.service.ApplicationService;
import com.abbtech.practice.lesson20.service.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl implements ApplicationService {
    private final CalculatorService calculatorService;

    public ApplicationServiceImpl(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }


    public int performOperation(OperationEnum operation, int a, int b) throws
            IllegalArgumentException, ArithmeticException {
        return switch (operation) {
            case ADD -> addition(a, b);
            case SUBTRACT -> subtract(a, b);
            case MULTIPLY -> multiply(a, b);
            case DIVISION -> division(a, b);
        };
    }

    private int multiply(int a, int b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException("a is grt 4, b is grt 6");
        }
        return calculatorService.multiply(a, b);
    }

    private int subtract(int a, int b) {
        var result = calculatorService.subtract(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("Result 2, 4, 6 not allowed");
        }
        return result;
    }

    private int addition(int a, int b) {
        if (!(a >= 0 && b >= 0)) {
            throw new IllegalArgumentException("The arguments must be positive");
        }
        return calculatorService.addition(a, b);
    }

    private int division(int a, int b) {
        if (a % 2 != 0 || b % 2 != 0) {
            throw new IllegalArgumentException("Both arguments must be divisible by 2");
        }
        if (b == 0) {
            throw new ArithmeticException("Division by zero!");
        }
        return calculatorService.division(a, b);
    }

}
