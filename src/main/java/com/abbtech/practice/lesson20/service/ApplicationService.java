package com.abbtech.practice.lesson20.service;

import com.abbtech.practice.lesson20.enumeration.OperationEnum;

public interface ApplicationService {
    int performOperation(OperationEnum operation, int a, int b);
}
