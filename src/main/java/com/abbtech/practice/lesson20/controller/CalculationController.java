package com.abbtech.practice.lesson20.controller;

import com.abbtech.practice.lesson20.dto.CalculationRequestDTO;
import com.abbtech.practice.lesson20.dto.CalculationResponseDTO;
import com.abbtech.practice.lesson20.service.ApplicationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calculation")
public class CalculationController {

    private final ApplicationService applicationService;

    public CalculationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @PostMapping
    public CalculationResponseDTO calculate(@RequestBody CalculationRequestDTO calculationRequestDTO) {
        return new CalculationResponseDTO(
                applicationService.performOperation(
                        calculationRequestDTO.operation(),
                        calculationRequestDTO.a(),
                        calculationRequestDTO.b())
        );
    }

}
