-- Create table for cards
CREATE TABLE exercise19.cards
(
    id          SERIAL PRIMARY KEY,
    card_number VARCHAR(255),
    expiry_date DATE
);

-- Create table for students
CREATE TABLE exercise19.students
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR(255),
    last_name  VARCHAR(255),
    card_id    BIGINT,
    FOREIGN KEY (card_id) REFERENCES exercise19.cards (id)
);

-- Create table for classes
CREATE TABLE exercise19.classes
(
    id         SERIAL PRIMARY KEY,
    class_name VARCHAR(255)
);

-- Create table for departments
CREATE TABLE exercise19.departments
(
    id              SERIAL PRIMARY KEY,
    department_name VARCHAR(255)
);

-- Create table for books
CREATE TABLE exercise19.books
(
    id     SERIAL PRIMARY KEY,
    title  VARCHAR(255),
    author VARCHAR(255)
);

-- Create table for professors
CREATE TABLE exercise19.professors
(
    id            SERIAL PRIMARY KEY,
    first_name    VARCHAR(255),
    last_name     VARCHAR(255),
    department_id BIGINT,
    book_id       BIGINT,
    FOREIGN KEY (department_id) REFERENCES exercise19.departments (id),
    FOREIGN KEY (book_id) REFERENCES exercise19.books (id)
);

-- Create table for student_class (many-to-many relationship between students and classes)
CREATE TABLE exercise19.student_class
(
    student_id BIGINT,
    class_id   BIGINT,
    PRIMARY KEY (student_id, class_id),
    FOREIGN KEY (student_id) REFERENCES exercise19.students (id),
    FOREIGN KEY (class_id) REFERENCES exercise19.classes (id)
);


